<?php

namespace Skimia\Auth;

use Orchestra\Model\Role;
use Skimia\Auth\Facades\AclActions;
use Skimia\Auth\Managers\AclManager;
use Skimia\Modules\ModuleBase;
use Event;
use Artisan;
class Module extends ModuleBase{


    public function register(){
        parent::register();




    }

    public function beforeRegisterModules(){
        Event::listen('artisan.os.install.migrate',function($command){
            $command->call('migrate', array('--force'=>true,'--package' => 'orchestra/memory'));
            $command->call('migrate', array('--force'=>true,'--package' => 'orchestra/auth'));
        },10000);


        Event::listen('artisan.os.install.seed',function($command){
            $command->call('db:seed', array('--force'=>true,'--class' => 'Skimia\Auth\Data\Seeds\UsersSeeder'));
        },10000);


        Event::listen('artisan.os.install.finish',function($command){
            $command->call('os:acl:install');
        },10000);

        Event::listen('artisan.os.install.acl',function($manager){
            $manager->attachRole(Role::admin());
            $manager->attachRole(Role::member());

        },10000);
    }


    public function getAliases(){
        return [
            'Orchestra\Acl' => 'Orchestra\Support\Facades\Acl',
            'User' => 'Skimia\Auth\Data\Models\User',
        ];
    }

} 