<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 08/03/2015
 * Time: 12:21
 */
namespace Skimia\Auth\Managers;

use App;
use Orchestra\Model\Role;
use Orchestra\Support\Facades\Memory;

class AclManager {

    protected $acl = false;

    public function __construct($acl){
        $this->acl = $acl;
    }


    public function save(){

        $this->acl->sync();
        Memory::finish();
    }

    public function attachRole(Role $role){

        $this->acl->roles()->attach([$role->name]);
    }


    public function attachActions( $arr, $allow = false){


        $this->acl->actions()->attach($arr);


        if($allow){
            $this->allowAll($arr);
            return;
        }


        $admin = Role::admin();

        //Set ACL metric for administrator
        if($this->acl->roles()->has($admin->name))
            $this->acl->allow($admin->name, $arr);


    }

    public function allowAll($arr){
        $roles = Role::all()->toArray();

        foreach ($roles as $role) {
            $this->acl->allow($role['name'], $arr);
        }

    }
    public function removeActions($arr){


        $this->acl->actions()->detach($arr);


    }

    public function getAcl(){
        return $this->acl;
    }
}