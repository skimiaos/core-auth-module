<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 11/11/2014
 * Time: 15:02
 */
namespace Skimia\Auth\Traits;

use App;
trait Acl{
    /**
     * @return \Orchestra\Auth\Acl\Container
     */
    protected function getAcl(){
        return App::make('acl');
    }
}