<?php

return [
    'name'        => 'Auth',
    'author'      => 'Skimia',
    'description' => 'Acl et géstion utilisateur',
    'namespace'   => 'Skimia\\Auth',
];